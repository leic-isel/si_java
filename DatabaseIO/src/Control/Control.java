package Control;

import DataAccessObject.DAOfacade;
import DataTransferObject.Colono;
import DataTransferObject.Pessoa;

import java.sql.SQLException;

public class Control {
    static DAOfacade daoFacade = new DAOfacade();

    public static void control(){}

    public static String[] insertPessoa(Pessoa pessoa) throws SQLException, ClassNotFoundException {

        // TODO falta ver se a pessoa já existe na BD
        return daoFacade.insertPessoa(pessoa);
    }

    public static String[] getEquipa(int monitor) throws SQLException, ClassNotFoundException {
        return daoFacade.query("SELECT numero, grupo, designacao, monitor FROM CAMPO_FERIAS.dbo.EQUIPA WHERE monitor = " + monitor + ";", 4);
    }

    public static int getLastPessoa(String nome, String endereco, String ntelefone, String email) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT numero FROM CAMPO_FERIAS.dbo.PESSOA WHERE "
                        + "nome = '" + nome + "' AND "
                        + "endereco = '" + endereco + "' AND "
                        + "ntelefone = '" + ntelefone + "' AND "
                        + "email = '" + email + "';";
        int res = Integer.parseInt(daoFacade.query(sqlQuery, 1)[0]);
        return res;
    }

    public static void insertColono(Colono colono) throws SQLException, ClassNotFoundException {
        DAOfacade.insertColono(colono);
    }

    public static String[] getColonos() throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT numero, nome, dtnascimento FROM CAMPO_FERIAS.dbo.COLONO;";
        return daoFacade.query(sqlQuery, 3);
    }

    public static String[] getEquipas() throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT numero, designacao FROM CAMPO_FERIAS.dbo.EQUIPA;";
        return daoFacade.query(sqlQuery, 2);
    }

    public static void changeColonoFromTeam(int colono, int equipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.COLONO SET equipa = " + equipa + " WHERE numero = " + colono + ";";
        daoFacade.query(sqlQuery, 0);
    }

    public static String[] getAtividadesPorEquipas() throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT A.referencia, designacao, descricao, horainicial, horafinal, equipa "
                        + "FROM CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA "
                        + "JOIN ACTIVIDADE A ON A.referencia = ACTIVIDADE_EQUIPA.referencia;";
        return daoFacade.query(sqlQuery, 6);
    }

    public static void deleteActividade(int referencia, int equipa) throws SQLException, ClassNotFoundException {
        DAOfacade.deleteActividadeEquipa(referencia, equipa);
    }

    public static String[] getPessoaMonitor() throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT PESSOA.numero, nome FROM CAMPO_FERIAS.dbo.PESSOA JOIN CAMPO_FERIAS.dbo.MONITOR "
                        + "ON PESSOA.numero = MONITOR.numero";
        return daoFacade.query(sqlQuery, 2);
    }

    public static void deleteMonitor(int numero) throws SQLException, ClassNotFoundException {
        DAOfacade.deleteMonitor(numero);
    }

    public static String[] getPessoaMonitorEquipa() throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT PESSOA.numero, nome, designacao, E.numero FROM PESSOA "
                        + "JOIN ON PESSOA.numero = MONITOR.numero "
                        + "LEFT JOIN EQUIPA E ON MONITOR.numero = E.numero;";

        return daoFacade.query(sqlQuery, 4);
    }

    public static void changeMonitorFromTeam(int equipa, Integer monitor) throws SQLException, ClassNotFoundException {
        daoFacade.updateMonitorEquipa(equipa, monitor);
    }

    public static String[] getMinParticipantes(int minParticipantes) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT designacao, descricao, participantes FROM ACTIVIDADE_DESPORTIVA "
                        + "JOIN ACTIVIDADE A ON ACTIVIDADE_DESPORTIVA.referencia = A.referencia "
                        + "WHERE participantes >= " + minParticipantes + ";";

        return daoFacade.query(sqlQuery, 3);
    }

    public static String[] getActividadesGrupo(String grupo) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT grupo, A.designacao, descricao, horainicial, horafinal "
                        + "FROM ACTIVIDADE_EQUIPA JOIN EQUIPA E ON E.numero = ACTIVIDADE_EQUIPA.equipa "
                        + "JOIN ACTIVIDADE A ON ACTIVIDADE_EQUIPA.referencia = A.referencia "
                        + "WHERE grupo = '" + grupo + "';";

        return daoFacade.query(sqlQuery, 5);
    }

    public static void changeActividadeTimeRestriction() throws SQLException, ClassNotFoundException {
        String sqlQuery = "ALTER TABLE ACTIVIDADE_EQUIPA DROP CONSTRAINT CHK_HF2;";
        daoFacade.query(sqlQuery, 0);
        sqlQuery = "ALTER TABLE ACTIVIDADE_EQUIPA ADD CONSTRAINT CHK_HF2 CHECK (DATEDIFF(minute, [horainicial], [horafinal]) <= 120);";
        daoFacade.query(sqlQuery, 0);
    }

    public static String[] actividadesNotRealizedBetween(String horainicial, String horafinal) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT designacao, descricao, equipa, horainicial, horafinal FROM ACTIVIDADE_EQUIPA "
                        + "JOIN ACTIVIDADE A ON A.referencia = ACTIVIDADE_EQUIPA.referencia WHERE "
                        + "(horainicial NOT BETWEEN '" + horainicial + "' AND '" + horafinal + "') AND "
                        + "(horafinal NOT BETWEEN '" + horainicial + "' AND '" + horafinal + "');";
        return daoFacade.query(sqlQuery, 5);
    }

    public static String[] eEducacaoWithMoreThanXColonos(int nColonos) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT P.nome, COUNT(*) FROM COLONO JOIN PESSOA P ON COLONO.eeducacao = P.numero "
                        + "GROUP BY P.nome HAVING COUNT(*) > " + nColonos + ";";
        return  daoFacade.query(sqlQuery, 2);
    }

    public static String[] representantesOfMoreThanXColonos(int nColonos) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT"; // TODO
        return daoFacade.query(sqlQuery, 2);
    }
}

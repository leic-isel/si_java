package DataAccessObject;

import DataTransferObject.*;
import java.sql.SQLException;

public class DAOfacade {

    public DAOfacade() {}

    public static String[] query(String sqlQuery, int sizeOfRow) throws SQLException, ClassNotFoundException {
        //String url = "jdbc:sqlserver://campoferias.database.windows.net:1433;DatabaseName=campo_ferias;user=vladimir;password=Yd4xee35$";
        String url = "jdbc:sqlserver://localhost:1433;user=si1su;password=#_su!si1;databaseName=campo_ferias";
        DatabaseIO databaseIO = new DatabaseIO(sqlQuery, url);

        return databaseIO.getResults(sizeOfRow);
    }

    // TABLE ACTIVIDADE -----------------------------------------------------------------------------------------------
    public String[] insertActividade(Actividade actividade) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT into CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao)"
                        + " VALUES ('"
                        + actividade.getDesignacao() + "', '"
                        + actividade.getDescricao() + "', "
                        + actividade.getDuracao() + ", '"
                        + actividade.getParticipacao() + "');";

        return query(sqlQuery, 1);
    }

    public String[] getReferenciaActividade(Actividade actividade) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT referencia FROM CAMPO_FERIAS.dbo.ACTIVIDADE WHERE "
                        + "designacao = '" + actividade.getDesignacao() + "' AND "
                        + "descricao = '" + actividade.getDescricao() + "' AND "
                        + "duracao = " + actividade.getDuracao() + " AND "
                        + "participacao = '" + actividade.getParticipacao() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateActividade(int referencia, Actividade newActividade) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.ACTIVIDADE SET "
                        + "designacao = '" + newActividade.getDesignacao() +"', "
                        + "descricao = '" + newActividade.getDescricao() + "', "
                        + "duracao = " + newActividade.getDuracao() + ", "
                        + "participacao = '" + newActividade.getParticipacao() + " "
                        + "WHERE referencia = " + referencia + ";";

        return query(sqlQuery, 1);
    }

    public String[] deleteActividade(int referencia) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.ACTIVIDADE WHERE referencia = " + referencia + ";";

        return query(sqlQuery, 1);
    }

    // TABLE ACTIVIDADE_DESPORTIVA ------------------------------------------------------------------------------------
    public String[] insertActividadeDesportiva(ActividadeDesportiva actividadeDesportiva) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT into CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes)"
                        + " VALUES ('"
                        + actividadeDesportiva.getReferencia() + "', '"
                        + actividadeDesportiva.getParticipantes() + "');";

        return query(sqlQuery, 1);
    }

    public String[] updateParticipantesActividadeDesportiva(int referencia, int participantes) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA SET "
                        + "participantes = " + participantes +", "
                        + "WHERE referencia = " + referencia + ";";

        return query(sqlQuery, 1);
    }

    public String[] deleteActividadeDesportiva(int referencia, int participantes) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA WHERE"
                        + "referencia = " + referencia + " AND "
                        + "participantes = " + participantes + ";";

        return query(sqlQuery, 1);
    }

    // TABLE ACTIVIDADE_EQUIPA ----------------------------------------------------------------------------------------
    public String[] insertActividadeEquipa(ActividadeEquipa actividadeEquipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT into CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA (referencia, equipa, horainicial, horafinal)"
                        + " VALUES ("
                        + actividadeEquipa.getReferencia() + ", "
                        + actividadeEquipa.getEquipa() + ", '"
                        + actividadeEquipa.getHorainicial() + ", '"
                        + actividadeEquipa.getHorafinal() + "');";

        return query(sqlQuery, 1);
    }

    public String[] updateActividadeEquipa(ActividadeEquipa actividadeEquipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA SET"
                        + "equipa = " + actividadeEquipa.getEquipa() + ", "
                        + "horainicial = '" + actividadeEquipa.getHorainicial() + "', "
                        + "horafinal = '" + actividadeEquipa.getHorafinal() + "' "
                        + "WHERE referencia = " + actividadeEquipa.getReferencia() + ";";

        return query(sqlQuery, 1);
    }

    public static String[] deleteActividadeEquipa(int referencia, int equipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA WHERE "
                        + "referencia = " + referencia + " AND equipa = " + equipa + ";";

        return query(sqlQuery,0);
    }

    // TABLE ACTIVIDADE_MONITOR ---------------------------------------------------------------------------------------
    public String[] insertActividadeMonitor(ActividadeMonitor actividadeMonitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR (referencia, monitor, equipa) VALUES ("
                        + actividadeMonitor.getReferencia() + ", "
                        + actividadeMonitor.getMonitor() + ", "
                        + actividadeMonitor.getEquipa() + ");";

        return query(sqlQuery, 1);
    }

    public String[] updateReferenciaActividadeMonitor(int referencia, ActividadeMonitor actividadeMonitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR SET "
                        + "referencia = " + referencia + " WHERE "
                        + "monitor = " + actividadeMonitor.getMonitor() + " AND "
                        + "equipa = " + actividadeMonitor.getEquipa() + ";";

        return query(sqlQuery,1);
    }

    public String[] updateMonitorActividadeMonitor(int monitor, ActividadeMonitor actividadeMonitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR SET "
                + "monitor = " + monitor + " WHERE "
                + "referencia = " + actividadeMonitor.getReferencia() + " AND "
                + "equipa = " + actividadeMonitor.getEquipa() + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateEquipaActividadeMonitor(int equipa, ActividadeMonitor actividadeMonitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR SET "
                + "equipa = " + equipa + " WHERE "
                + "monitor = " + actividadeMonitor.getMonitor() + " AND "
                + "referencia = " + actividadeMonitor.getReferencia() + ";";

        return query(sqlQuery, 1);
    }

    public String[] deleteActividadeMonitor(ActividadeMonitor actividadeMonitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR WHERE "
                        + "referencia = " + actividadeMonitor.getReferencia() + " AND "
                        + "monitor = " + actividadeMonitor.getMonitor() + " AND "
                        + "equipa = " + actividadeMonitor.getEquipa() + ";";

        return query(sqlQuery, 1);
    }

    // TABLE CAMPOFERIAS ----------------------------------------------------------------------------------------------
    public String[] getCampoFerias() throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT * FROM CAMPO_FERIAS.dbo.CAMPOFERIAS;";

        return query(sqlQuery, 8);
    }
    public String[] insertCampoFerias(CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.CAMPOFERIAS (codigo, nome, endereco, localidade, "
                        + "codpostal, enderecoweb, latitude, longitude) VALUES ('"
                        + campoFerias.getCodigo() + "', '"
                        + campoFerias.getNome() + "', '"
                        + campoFerias.getEnderco() + "', '"
                        + campoFerias.getLocalidade() + "', '"
                        + campoFerias.getCodpostal() + "', '"
                        + campoFerias.getEnderecoweb() + "', "
                        + campoFerias.getLatitude() + ", "
                        + campoFerias.getLongitude() + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateCodigoCampoFerias(String codigo, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET codigo = '" + codigo + "' WHERE "
                        + "nome = '" + campoFerias.getNome() + "' AND "
                        + "endereco = '" + campoFerias.getEnderco() + "' AND "
                        + "localidade = '" + campoFerias.getLocalidade() + "' AND "
                        + "codpostal = '" + campoFerias.getCodpostal() + "' AND "
                        + "enderecoweb = '" + campoFerias.getEnderecoweb() + "' AND"
                        + "latitude = " + campoFerias.getLatitude() + " AND "
                        + "longitude = " + campoFerias.getLongitude() + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateNomeCampoFerias(String nome, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET nome = '" + nome + "' WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateEnderecoCampoFerias(String endereco, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET endereco = '" + endereco + "' WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateLocalidadeCampoFerias(String localidade, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET localidade = '" + localidade + "' WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateCodpostalCampoFerias(String codpostal, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET codpostal = '" + codpostal + "' WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateEnderecowebCampoFerias(String enderecoweb, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET enderecoweb = '" + enderecoweb + "' WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateLatitudeCampoFerias(float latitude, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET latitude = " + latitude + " WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateLongitudeCampoFerias(float longitude, CampoFerias campoFerias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.CAMPOFERIAS SET longitude = " + longitude + " WHERE "
                + "codigo = '" + campoFerias.getCodigo() + "';";

        return query(sqlQuery, 1);
    }

    public String[] deleteCampoFerias(String codigo) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.CAMPOFERIAS WHERE codigo = '" + codigo + "';";

        return query(sqlQuery, 1);
    }

    // TABLE COLONO ---------------------------------------------------------------------------------------------------
    public static String[] insertColono(Colono colono) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.COLONO "
                        + "(nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa)"
                        + " VALUES ('"
                        + colono.getNome() + "', '"
                        + colono.getDtnascimento() + "', '"
                        + colono.getContacto() + "', "
                        + colono.getEscolaridade() + ", '"
                        + colono.getCcidadao() + "', "
                        + colono.getCutente() + ", "
                        + colono.getEeducacao() + ", "
                        + colono.getEquipa() + ");";

        return query(sqlQuery, 0);
    }

    public String[] getNumeroColono(Colono colono) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT numero FROM CAMPO_FERIAS.dbo.COLONO WHERE "
                        + "nome = '" + colono.getNome() + "' AND "
                        + "dtnascimento = '" + colono.getDtnascimento() + "' AND "
                        + "contacto = '" + colono.getContacto() + "' AND "
                        + "escolaridade = " + colono.getEscolaridade() + " AND "
                        + "ccidadao = '" + colono.getCcidadao() + "' AND "
                        + "cutente = " + colono.getCutente() + " AND "
                        + "eeducacao = " + colono.getEeducacao() + "AND "
                        + "equipa = " + colono.getEquipa() + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateNomeColono(String nome, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                        + "SET nome = '" + nome + "' WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateDtnascimentoColono(String dtnascimento, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET dtnascimento = '" + dtnascimento + "' WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateContactoColono(String contacto, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET contacto = '" + contacto + "' WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateEscolaridadeColono(int escolaridade, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET escolaridade = " + escolaridade + " WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateCcidadaoColono(String ccidadao, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET ccidadao = '" + ccidadao + "' WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateCutenteColono(long cutente, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET cutente = " + cutente + " WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateEeducacaoColono(int eeducacao, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET eeducacao = " + eeducacao + " WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateEquipaColono(int equipa, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dto.COLONO "
                + "SET equipa = " + equipa + " WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] deleteColono(int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.COLONO WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    // TABLE CONTACTO -------------------------------------------------------------------------------------------------
    public String[] insertContacto(Contacto contacto) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.CONTACTO (identificador, contacto, descricao)"
                        + " VALUES ('"
                        + contacto.getIdentificador() + "', '"
                        + contacto.getContacto() + "', '"
                        + contacto.getDescricao() + "';";

        return query(sqlQuery, 1);
    }

    // TODO if necessary updateContacto()

    public String[] deleteContacto(String identificador, String contacto) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.CONTACTO WHERE identificador = '"
                        + identificador + "' AND contacto = '" + contacto + "';";

        return query(sqlQuery, 1);
    }

    // TABLE EQUIPA ---------------------------------------------------------------------------------------------------
    public String[] insertEquipa(Equipa equipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.EQUIPA (grupo, designacao, monitor) VALUES ('"
                        + equipa.getGrupo() + "', '"
                        + equipa.getDesignacao() + "', "
                        + equipa.getMonitor() + ";";

        return query(sqlQuery, 1);
    }

    public String[] getNumeroEquipa(Equipa equipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT numero FROM CAMPO_FERIAS.dbo.EQUIPA WHERE "
                        + "grupo = '" + equipa.getGrupo() + "' AND "
                        + "designacao = '" + equipa.getDesignacao() + "' AND "
                        + "monitor = '" + equipa.getMonitor() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateGrupoEquipa(int numero, String grupo) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.EQUIPA SET grupo = '" + grupo
                        + "' WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateDesignacaoEquipa(int numero, String designacao) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.EQUIPA SET designacao = '" + designacao
                        + "' WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public static String[] updateMonitorEquipa(int numero, int monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.EQUIPA SET monitor = " + monitor
                + " WHERE numero = " + numero + ";";

        return query(sqlQuery, 0);
    }

    public String[] deleteEquipa(int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.EQUIPA WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    // TABLE GRUPO ----------------------------------------------------------------------------------------------------
    public String[] insertGrupo(Grupo grupo) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.GRUPO (nome, idademinima, idademaxima)"
                        + " VALUES ('"
                        + grupo.getNome() + "', "
                        + grupo.getIdademinima() + "' ,"
                        + grupo.getIdademaxima() + "');";

        return query(sqlQuery, 1);
    }

    // TODO update methods if necessary

    public String[] deleteGroup(String nome) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.GRUPO WHERE nome = '" + nome + "';";

        return query(sqlQuery, 1);
    }

    // TABLE MONITOR --------------------------------------------------------------------------------------------------
    public String[] insertMonitor(Monitor monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.MONITOR"
                        + " (numero, idade, escolaridade, comonitor, cpferias) VALUES ("
                        + monitor.getNumero() + ", "
                        + monitor.getIdade() + ", '"
                        + monitor.getEscolaridade() + "', "
                        + monitor.getComonitor() + ", '"
                        + monitor.getCpferias() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateNumeroMonitor(int numero, Monitor monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.MONITOR SET numero = " + numero + " WWHERE "
                        + "idade = " + monitor.getIdade() + " AND "
                        + "escolaridade = '" + monitor.getEscolaridade() + "' AND "
                        + "comonitor = " + monitor.getComonitor() + " AND "
                        + "cpferias = '" + monitor.getCpferias() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateIdadeMonitor(int idade, Monitor monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.MONITOR SET idade = " + idade + " WWHERE "
                        + "numero = " + monitor.getNumero() + " AND "
                        + "escolaridade = '" + monitor.getEscolaridade() + "' AND "
                        + "comonitor = " + monitor.getComonitor() + " AND "
                        + "cpferias = '" + monitor.getCpferias() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateEscolaridadeMonitor(String escolaridade, Monitor monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.MONITOR SET escolaridade = '" + escolaridade + "' WWHERE "
                        + "idade = " + monitor.getIdade() + " AND "
                        + "numero = " + monitor.getNumero() + " AND "
                        + "comonitor = " + monitor.getComonitor() + " AND "
                        + "cpferias = '" + monitor.getCpferias() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateComonitorMonitor(int comonitor, Monitor monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.MONITOR SET comonitor = " + comonitor + " WWHERE "
                        + "idade = " + monitor.getIdade() + " AND "
                        + "numero = " + monitor.getNumero() + " AND "
                        + "numero = " + monitor.getNumero() + " AND "
                        + "cpferias = '" + monitor.getCpferias() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateCpferiasMonitor(String cpferias, Monitor monitor) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.MONITOR SET cpferias = '" + cpferias + "' WWHERE "
                        + "idade = " + monitor.getIdade() + " AND "
                        + "numero = " + monitor.getNumero() + " AND "
                        + "comonitor = " + monitor.getComonitor() + " AND "
                        + "numero = " + monitor.getNumero() + ";";

        return query(sqlQuery, 1);
    }

    public static String[] deleteMonitor(int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.MONITOR WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    // TABLE PESSOA ---------------------------------------------------------------------------------------------------
    public String[] insertPessoa(Pessoa pessoa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email)"
                        + " VALUES ('"
                        + pessoa.getNome() + "', '"
                        + pessoa.getEndereco() + "', '"
                        + pessoa.getNtelefone() + "', '"
                        + pessoa.getEmail() + "');";

        return query(sqlQuery, 1);
    }

    public String[] getNumeroPessoa(Pessoa pessoa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT numero FROM CAMPO_FERIAS.dbo.PESSOA WHERE "
                        + "nome = '" + pessoa.getNome() + "' AND "
                        + "endereco = '" + pessoa.getEndereco() + "' AND "
                        + "ntelefone = '" + pessoa.getNtelefone() + "' AND "
                        + "email = '" + pessoa.getEmail() + "';";

        return query(sqlQuery, 1);
    }

    public String[] updateNomePessoa(String nome, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.PESSOA SET nome = '" + nome + "' WWHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateEnderecoPessoa(String endereco, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.PESSOA SET endereco = '" + endereco + "' WWHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateNtelefonePessoa(String ntelefone, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.PESSOA SET ntelefone = '" + ntelefone + "' WWHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] updateEmailPessoa(String email, int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "UPDATE CAMPO_FERIAS.dbo.PESSOA SET email = '" + email + "' WWHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    public String[] deletePessoa(int numero) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.PESSOA WHERE numero = " + numero + ";";

        return query(sqlQuery, 1);
    }

    // TABLE REPRESENTANTE --------------------------------------------------------------------------------------------
    public String[] insertRepresentante(Representante representante) throws SQLException, ClassNotFoundException {
        String sqlQuery = "INSERT INTO CAMPO_FERIAS.dbo.REPRESENTANTE (colono, equipa) VALUES "
                        + representante.getColono() + ", "
                        + representante.getEquipa() + ";";

        return query(sqlQuery, 1);
    }

    // TODO se necessario fazer updateRepresentante()

    public String[] deleteRepresentante(int colono, int equipa) throws SQLException, ClassNotFoundException {
        String sqlQuery = "DELETE FROM CAMPO_FERIAS.dbo.REPRESENTANTE WHERE "
                        + "colono = " + colono + " AND equipa = " + equipa + ";";

        return query(sqlQuery, 1);
    }


    public String[] getPessoaMonitorbyCampoFerias(String cpferias) throws SQLException, ClassNotFoundException {
        String sqlQuery = "SELECT PESSOA.numero, nome FROM CAMPO_FERIAS.dbo.PESSOA "
                        + "JOIN MONITOR ON PESSOA.numero = MONITOR.numero "
                        + "WHERE cpferias = '" + cpferias + "';";

        return query(sqlQuery, 2);
    }
}

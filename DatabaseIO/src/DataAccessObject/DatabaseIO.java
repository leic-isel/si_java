package DataAccessObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Arrays;

import com.microsoft.sqlserver.jdbc.SQLServerDriver;

public class DatabaseIO {
    private String sqlQuery, url;
    public DatabaseIO(String sqlQuery, String url) {
        this.sqlQuery = sqlQuery;
        this.url = url;
    }

    public String[] getResults(int sizeOfRow) throws SQLException, ClassNotFoundException {
        String[] results = new String[sizeOfRow];
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // Driver JDBC-ODBC Bridge: carregar e registar driver
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

            // Driver Tipo 4 para SQLServer
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (Exception qsqlex){
            System.out.println("Erro: " + qsqlex.getMessage());
        }

        try{
            con = DriverManager.getConnection(url); // Estabelecer ligacao
            stmt = con.createStatement(); // Executar comando
            rs = stmt.executeQuery(sqlQuery);

            // iterar resultado
            boolean firstiteraction = true;
            int j = 0, k = 1;
            while(rs.next()){
                if (! firstiteraction) {
                    String[] temp = Arrays.copyOf(results, results.length);
                    results = Arrays.copyOf(temp, temp.length + sizeOfRow);
                }

                for (int i=1; j < sizeOfRow * k; j++, i++) {
                    results[j] = rs.getString(i);
                }
                firstiteraction = false;
                k++;
            }
        }
        catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
        }
        finally {
            if(rs != null) rs.close();  // libertar recursos do ResultSet
            if(stmt != null) stmt.close(); // libertar rescursos do Statment
            if(con != null) con.close(); // fechar a ligação
        }
        return results;
    }

}

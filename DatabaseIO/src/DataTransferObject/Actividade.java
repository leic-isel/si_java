package DataTransferObject;

public class Actividade {
    private int referencia, duracao;
    private String designacao, descricao, participacao;

    private enum Designacao {
        DESPORTIVA ("desportiva"),
        RECREATIVA ("recreativa"),
        AVENTURA ("aventura"),
        RADICAIS ("desportos radicais"),
        AMBIENTAIS ("ambientais"),
        ARTES ("artes"),
        EDUCACAO ("educacao");

        Designacao(String designacao){}
    }

    private enum Participacao {
        OPCIONAL ("opcional"),
        OBRIGATORIA ("obrigatoria");

        Participacao(String participacao){}
    }

    public Actividade(){};

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public void setDuracao(int duracao) { if (duracao > 0 && duracao < 240) this.duracao = duracao; }

    public void setParticipacao(String participacao) { this.participacao = participacao; }

    public int getReferencia() {
        return referencia;
    }

    public String getDesignacao() {
        return designacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getDuracao() {
        return duracao;
    }

    public String getParticipacao() {
        return participacao;
    }
}

package DataTransferObject;

public class ActividadeDesportiva {
    private int participantes, referencia;  // FK Actividade (referencia)

    public ActividadeDesportiva(){};

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public void setParticipantes(int participantes) {
        this.participantes = participantes;
    }

    public int getReferencia() {
        return referencia;
    }

    public int getParticipantes() {
        return participantes;
    }
}

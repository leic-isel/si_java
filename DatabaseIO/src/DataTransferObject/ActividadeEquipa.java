package DataTransferObject;

import java.time.LocalTime;

public class ActividadeEquipa {
    private int referencia, equipa; // FK Actividade (referencia), FK Equipa (numero)
    private LocalTime horainicial, horafinal;

    public ActividadeEquipa(){};

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public void setEquipa(int equipa) {
        this.equipa = equipa;
    }

    public void setHorainicial(LocalTime horainicial) { this.horainicial = horainicial; }

    public void setHorafinal(LocalTime horafinal) {
        this.horafinal = horafinal;
    }

    public int getReferencia() {
        return referencia;
    }

    public int getEquipa() {
        return equipa;
    }

    public LocalTime getHorainicial() {
        return horainicial;
    }

    public LocalTime getHorafinal() {
        return horafinal;
    }
}

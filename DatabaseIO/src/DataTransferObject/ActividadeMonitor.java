package DataTransferObject;

public class ActividadeMonitor {
    private int referencia, monitor, equipa;  // FK Actividade (referencia), FK Monitor (numero), FK Equipa (numero)

    public ActividadeMonitor(){};

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public void setMonitor(int monitor) {
        this.monitor = monitor;
    }

    public void setEquipa(int equipa) {
        this.equipa = equipa;
    }

    public int getReferencia() {
        return referencia;
    }

    public int getMonitor() {
        return monitor;
    }

    public int getEquipa() {
        return equipa;
    }
}

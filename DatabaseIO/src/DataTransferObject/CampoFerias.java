package DataTransferObject;

public class CampoFerias {
    private String codigo, nome, endereco, localidade, codpostal, enderecoweb;
    private float latitude, longitude;

    public CampoFerias() {};

    public void setCodigo(String codigo) { this.codigo = codigo; }

    public void setNome(String nome) { this.nome = nome; }

    public void setEnderco(String enderco) { this.endereco = enderco; }

    public void setLocalidade(String localidade) { this.localidade = localidade; }

    public void setCodpostal(String codpostal) { this.codpostal = codpostal; }

    public void setEnderecoweb(String enderecoweb) { this.enderecoweb = enderecoweb; }

    public void setLatitude(float latitude) { this.latitude = latitude; }

    public void setLongitude(float longitude) { this.longitude = longitude; }

    public String getCodigo() { return codigo; }

    public String getNome() { return nome; }

    public String getEnderco() { return endereco; }

    public String getLocalidade() { return localidade; }

    public String getCodpostal() { return codpostal; }

    public String getEnderecoweb() { return enderecoweb; }

    public float getLatitude() { return latitude; }

    public float getLongitude() { return longitude; }
}

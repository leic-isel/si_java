package DataTransferObject;

public class Colono {
    private String nome, dtnascimento, contacto, ccidadao;
    private int escolaridade, eeducacao, equipa; // FK Pessoa (numero), FK Equipa (numero)
    private double cutente;

    public Colono(){}

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDtnascimento(String dtnascimento) { this.dtnascimento = dtnascimento; }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setEscolaridade(int escolaridade) {
        this.escolaridade = escolaridade;
    }

    public void setCcidadao(String ccidadao) {
        this.ccidadao = ccidadao;
    }

    public void setCutente(double cutente) {
        this.cutente = cutente;
    }

    public void setEeducacao(int eeducacao) {
        this.eeducacao = eeducacao;
    }

    public void setEquipa(int equipa) {
        this.equipa = equipa;
    }

    public String getNome() {
        return nome;
    }

    public String getDtnascimento() {
        return dtnascimento;
    }

    public String getContacto() {
        return contacto;
    }

    public String getCcidadao() {
        return ccidadao;
    }

    public int getEscolaridade() {
        return escolaridade;
    }

    public double getCutente() {
        return cutente;
    }

    public int getEeducacao() {
        return eeducacao;
    }

    public int getEquipa() {
        return equipa;
    }
}

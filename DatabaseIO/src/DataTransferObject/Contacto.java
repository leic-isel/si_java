package DataTransferObject;

public class Contacto {
    private String descricao, contacto, identificador; // FK CampoFerias (codigo)
    private enum Descricao {
        FIXO ("fixo"),
        MOVEL ("movel"),
        EMAIL ("email");

        Descricao(String descricao){}
    }

    public Contacto(){};

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getIdentificador() {
        return identificador;
    }

    public String getContacto() {
        return contacto;
    }

    public String getDescricao() {
        return descricao;
    }
}

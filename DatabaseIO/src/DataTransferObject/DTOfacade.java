package DataTransferObject;

import DataAccessObject.DAOfacade;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.Date;

public class DTOfacade {
    private static final DAOfacade daofacade = new DAOfacade();
    public DTOfacade() {}

    public Actividade createActividade(String designacao, String descricao, int duracao, String participacao) {
        Actividade actividade = new Actividade();

        actividade.setDesignacao(designacao);
        actividade.setDescricao(descricao);
        actividade.setDuracao(duracao);
        actividade.setParticipacao(participacao);

        return actividade;
    }

    public ActividadeDesportiva createActividadeDesportiva(int referencia, int participantes) {
        ActividadeDesportiva actividadeDesportiva = new ActividadeDesportiva();

        actividadeDesportiva.setReferencia(referencia);
        actividadeDesportiva.setParticipantes(participantes);

        return actividadeDesportiva;
    }

    public ActividadeEquipa createActividadeEquipa(int referencia, int equipa, LocalTime horainicial, LocalTime horafinal) {
        ActividadeEquipa actividadeEquipa = new ActividadeEquipa();

        actividadeEquipa.setReferencia(referencia);
        actividadeEquipa.setEquipa(equipa);
        actividadeEquipa.setHorainicial(horainicial);
        actividadeEquipa.setHorafinal(horafinal);

        return actividadeEquipa;
    }

    public ActividadeMonitor createActividadeMonitor(int referencia, int monitor, int equipa) {
        ActividadeMonitor actividadeMonitor = new ActividadeMonitor();

        actividadeMonitor.setReferencia(referencia);
        actividadeMonitor.setMonitor(monitor);
        actividadeMonitor.setEquipa(equipa);

        return actividadeMonitor;
    }

    public CampoFerias createCampoFerias(String codigo, String nome, String endereco, String localidade, String codpostal, String enderecoweb, float latitude, float longitude) {
        CampoFerias campoFerias = new CampoFerias();

        campoFerias.setCodigo(codigo);
        campoFerias.setNome(nome);
        campoFerias.setEnderco(endereco);
        campoFerias.setLocalidade(localidade);
        campoFerias.setCodpostal(codpostal);
        campoFerias.setEnderecoweb(enderecoweb);
        campoFerias.setLatitude(latitude);
        campoFerias.setLongitude(longitude);

        return campoFerias;
    }

    public static Colono createColono(String nome, String dtnascimento, String contacto, int escolaridade, String ccidadao, long cutente, int eeducacao, int equipa) {
        Colono colono = new Colono();

        colono.setNome(nome);
        colono.setDtnascimento(dtnascimento);
        colono.setContacto(contacto);
        colono.setEscolaridade(escolaridade);
        colono.setCcidadao(ccidadao);
        colono.setCutente(cutente);
        colono.setEeducacao(eeducacao);
        colono.setEquipa(equipa);

        return colono;
    }

    public Contacto createContacto(String identificador, String contacto, String descricao) {
        Contacto contacto1 = new Contacto();

        contacto1.setIdentificador(identificador);
        contacto1.setContacto(contacto);
        contacto1.setDescricao(descricao);

        return contacto1;
    }

    public Equipa createEquipa(String grupo, String designacao, int monitor) {
        Equipa equipa = new Equipa();

        equipa.setGrupo(grupo);
        equipa.setDesignacao(designacao);
        equipa.setMonitor(monitor);

        return equipa;
    }

    public Grupo createGrupo(String nome, int idademinima, int idademaxima) {
        Grupo grupo = new Grupo();

        grupo.setNome(nome);
        grupo.setIdademinima(idademinima);
        grupo.setIdademaxima(idademaxima);

        return grupo;
    }

    public Monitor createMonitor(int numero, int idade, String escolaridade, int comonitor, String cpferias) {
        Monitor monitor = new Monitor();

        monitor.setNumero(numero);
        monitor.setIdade(idade);
        monitor.setEscolaridade(escolaridade);
        monitor.setComonitor(comonitor);
        monitor.setCpferias(cpferias);

        return monitor;
    }

    public static Pessoa createPessoa(String nome, String endereco, String ntelefone, String email) {
        Pessoa pessoa = new Pessoa();

        pessoa.setNome(nome);
        pessoa.setEndereco(endereco);
        pessoa.setNtelefone(ntelefone);
        pessoa.setEmail(email);

        return pessoa;
    }

    public Representante createRepresentante(int colono, int equipa) {
        Representante representante = new Representante();

        representante.setColono(colono);
        representante.setEquipa(equipa);

        return representante;
    }


    public static String[] getCampoFerias() throws SQLException, ClassNotFoundException {
        return daofacade.getCampoFerias();
    }

    public static String[] getPessoaMonitorbyCampoFerias(String cpferias) throws SQLException, ClassNotFoundException {
        return daofacade.getPessoaMonitorbyCampoFerias(cpferias);
    }
}

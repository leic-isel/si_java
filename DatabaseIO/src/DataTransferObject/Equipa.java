package DataTransferObject;

public class Equipa {
    private int numero, monitor; // FK Monitor (numero)
    private String designacao, grupo; // FK Grupo (nome)

    public Equipa(){};

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public void setMonitor(int monitor) {
        this.monitor = monitor;
    }

    public int getNumero() {
        return numero;
    }

    public String getGrupo() {
        return grupo;
    }

    public String getDesignacao() {
        return designacao;
    }

    public int getMonitor() {
        return monitor;
    }
}

package DataTransferObject;

public class Grupo {
    private String nome;
    private int idademinima, idademaxima;

    public Grupo(){};

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setIdademinima(int idademinima) {
        this.idademinima = idademinima;
    }

    public void setIdademaxima(int idademaxima) {
        this.idademaxima = idademaxima;
    }

    public String getNome() {
        return nome;
    }

    public int getIdademinima() {
        return idademinima;
    }

    public int getIdademaxima() {
        return idademaxima;
    }
}

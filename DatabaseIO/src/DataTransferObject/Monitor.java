package DataTransferObject;

public class Monitor {
    private int idade, numero, comonitor; // PK e FK PESSOA (numero), FK MONITOR (numero)
    private String escolaridade, cpferias; // FK CAMPOFERIAS(codigo)
    private enum Escolaridade {
        SECUNDARIO ("secundario"),
        SUPERIOR ("superior");

        Escolaridade(String escolaridade){}
    }

    public Monitor(){};

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    public void setComonitor(int comonitor) {
        this.comonitor = comonitor;
    }

    public void setCpferias(String cpferias) {
        this.cpferias = cpferias;
    }

    public int getNumero() {
        return numero;
    }

    public int getIdade() {
        return idade;
    }

    public int getComonitor() {
        return comonitor;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public String getCpferias() {
        return cpferias;
    }
}

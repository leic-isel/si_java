package DataTransferObject;

public class Pessoa {
    private String nome, endereco, ntelefone, email;

    public Pessoa(){}

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setNtelefone(String ntelefone) {
        this.ntelefone = ntelefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getNtelefone() { return ntelefone; }

    public String getEmail() {
        return email;
    }
}

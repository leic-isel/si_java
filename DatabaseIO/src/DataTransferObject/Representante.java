package DataTransferObject;

public class Representante {
    private int colono, equipa;

    public Representante(){}

    public void setColono(int colono) {
        this.colono = colono;
    }

    public void setEquipa(int equipa) {
        this.equipa = equipa;
    }

    public int getColono() {
        return colono;
    }

    public int getEquipa() {
        return equipa;
    }
}

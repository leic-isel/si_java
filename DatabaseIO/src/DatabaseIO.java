import java.sql.*;

public class DatabaseIO {
    public static void main(String[] arg) throws SQLException, ClassNotFoundException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // Driver JDBC-ODBC Bridge: carregar e registar driver
            //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

            // Driver Tipo 4 para SQLServer
            //DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (Exception qsqlex){
            System.out.println("Erro: " + qsqlex.getMessage());
        }

        try{
            String url = "jdbc:sqlserver://campoferias.database.windows.net:1433;DatabaseName=campo_ferias;user=vladimir;password=Yd4xee35$";
            con = DriverManager.getConnection(url); // Estabelecer ligacao
            stmt = con.createStatement(); // Executar comando
            rs = stmt.executeQuery("select * from campo_ferias.dbo.colono");

            // iterar resultado
            while(rs.next()) System.out.println(rs.getString(2));
            System.out.println();
        }
        catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
        }
        finally {
            if(rs != null) rs.close();  // libertar recursos do ResultSet
            if(stmt != null) stmt.close(); // libertar rescursos do Statment
            if(con != null) con.close(); // fechar a ligação
        }
    }

}

/*
    Uma aplicação Java (em modo linha de comandos) que permita realizar as seguintes operações:

        Opção para adicionar um novo colono ao sistema;
        Opção para permitir a troca de equipa por parte de um colono, garantindo que na mesma não é ultrapassado o limite de participantes imposto;
        Opção para cancelar uma actividade, garantindo que é consistente com a tabela ACTIVIDADE_EQUIPA;
        Opção para remover um monitor ao sistema;
        Opção para permitir a troca de monitor numa dada equipa;
        Opções que implementem as alíneas 2.c,
        (c)  Liste  as  actividades  de  caŕacter  “obrigat́orio”  com  um  ḿınimo  de  participantes igual a 5.
        2.f, (f )  Liste todas as actividades realizadas pelas equipas dos “iniciados”.
        2.g, (g)  Identifique  os  representantes  (nome)  que  s̃ao  responśaveis  por  mais  do  que  4
                  colonos no campo de f́erias.
        3.c e (c)  Apresente  o  nome  dos  encarregados  de  educa̧c̃ao  e  o  enderȩco  com  mais  do  que um educando.
                  Fa̧ca uso de uma interroga̧c̃ao correlacionada.
        3.e (e)  Mostre  as  actividades  que  ñao  se  realizaram  num  determinado  peŕıodo  de  tempo, por exemplo
                 entre as 11h e as 12h.
        (da Fase 2), garantindo que todos os parâmetros variáveis são alteráveis na interface com o utilizador
        e.g. em 2.c as actividades podem ser de carácter "obrigatório" ou "opcional" e o número de participantes
        é variável;
        em 2.f escolher a equipa que se pretende apresentar;
        Alterar a restrição de coluna ao atributo duracao de 240 minutos para 120 minutos na tabela ACTIVIDADE.
*/

import Views.*;

import java.sql.SQLException;
import java.util.Scanner;

class Main {


    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        try {
            int choice = 99;

            System.out.println("DATABASE IO - SI1 - TL31N13 - Grupo13");
            System.out.println();
            while (choice != 0) {
                System.out.println("Operações da base de dados: (escolha uma opção)");
                System.out.println("0 - Terminar programa");
                System.out.println("1 - Adicionar colono");
                System.out.println("2 - Mudar colono de equipa");
                System.out.println("3 - Cancelar actividade");
                System.out.println("4 - Remover monitor");
                System.out.println("5 - Alterar monitor de equipa");
                System.out.println("6 - Mostrar atividades com um minimo de X participantes");
                System.out.println("7 - Mostrar actividades por grupo");
                System.out.println("8 - Representantes de mais de X colonos");
                System.out.println("9 - Encarregados de Educação com mais de X educandos");
                System.out.println("10 - Actividades não realizadas em determinado periodo");
                System.out.println("11 - Alterar restrição de tempo da actividade");
                System.out.println();
                System.out.print("Introduza a escolha: ");
                choice = in.nextInt();
                in.nextLine();

                switch (choice) {
                    case 1:
                        System.out.println("Adicionar colono");
                        if (AddColono.addColono()) System.out.println("Colono adicionado com sucesso");
                        else System.out.println("Falha ao adicionar colono!");
                        break;
                    case 2:
                        System.out.println("Trocar colono de equipa");
                        if (ChangeEquipa.changeEquipa()) System.out.println("Colono trocado com sucesso");
                        else System.out.println("Falha ao trocar colono!");
                        break;
                    case 3:
                        System.out.println("Cancelar actividade");
                        if (CancelActividade.cancelActividade()) System.out.println("Actividade cancelada com successo");
                        else System.out.println("Falha ao cancelar actividade!");
                        break;
                    case 4:
                        System.out.println("Remover monitor");
                        if (RemoveMonitor.removeMonitor()) System.out.println("Monitor removido com sucesso");
                        else System.out.println("Falha ao remover monitor!");
                        break;
                    case 5:
                        System.out.println("Alterar monitor de equipa");
                        if (ChangeMonitorFromTeam.changeMonitor()) System.out.println("Monitor alterado com sucesso");
                        else System.out.println("Falha ao alterar monitor!");
                        break;
                    case 6:
                        System.out.println("Pesquisar actividades com um mínimo de X participantes");
                        if (ShowActividadeParticipantes.showParticipantes()) System.out.println();
                        else System.out.println("Falha em mostrar actividades!");
                        break;
                    case 7:
                        System.out.println("Pesquisar actividades por grupo");
                        if (ActividadesPorGrupo.actividadesPorGrupo()) System.out.println();
                        else System.out.println("Falha em mostrar actividades!");
                        break;
                    case 8:
                        System.out.println("Representantes de mais de X colonos");
                        if (RepresentantesOfMoreThanXColonos.representantesOfMoreThanXcolonos()) System.out.println();
                        else System.out.println("Falha em mostrar representantes!");
                        break;
                    case 9:
                        System.out.println("Encarregados de educação com mais de X educandos");
                        if (EeducaçãoWithMoreThanXColonos.eEducacaoWithMoreThanXColonos()) System.out.println();
                        else System.out.println("Falha em mostrar encarregados de educação!");
                        break;
                    case 10:
                        System.out.println("Actividades não realizadas em X período de tempo");
                        if (ActividadesNotRealizedBetween.actividadesNotRealizedBetween()) System.out.println();
                        else System.out.println("Falha em mostrar actividades");
                        break;
                    case 11:
                        System.out.println("Alterar restrição temporal de actividade");
                        if (ChangeActividadeTimeRestriction.changeActividadeTimeRestriction())
                            System.out.println("Restrição temporal alterada com sucesso");
                        else System.out.println("Falha em alterar restrição temporal");
                        break;
                }
                System.out.println("Prima uma tecla para continuar");
                in.nextLine();
            }
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }
}

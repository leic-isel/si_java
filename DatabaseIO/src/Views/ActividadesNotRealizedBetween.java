package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class ActividadesNotRealizedBetween {
    private static final Scanner in = new Scanner(System.in);
    private static String horainicial, horafinal;

    public static boolean actividadesNotRealizedBetween() throws SQLException, ClassNotFoundException {
        try {
            setHoraInicial();
            setHoraFinal();
            showActividades();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setHoraInicial() {
        System.out.print("Escolha a hora inicial (HH:MM:SS): ");
        horainicial = in.nextLine();
    }

    private static void setHoraFinal() {
        System.out.print("Escolha a hora final (HH:MM:SS): ");
        horafinal = in.nextLine();
    }

    private static void showActividades() throws SQLException, ClassNotFoundException {
        String[] actividadesQuery = Control.actividadesNotRealizedBetween(horainicial, horafinal);
        System.out.println("Actividades\n");
        System.out.println(" |   Actividade   |  Descrição  | Equipa | Hora Inicial | Hora Final ");
        int j = 0, rowSize = 5;
        for (int i = 0; i < actividadesQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + actividadesQuery[j]);
            }
            System.out.print("\n");
        }
    }
}

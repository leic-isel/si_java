package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class ActividadesPorGrupo {
    private static final Scanner in = new Scanner(System.in);
    private static  String grupo;

    public static boolean actividadesPorGrupo() throws SQLException, ClassNotFoundException{
        try {
            setGrupo();
            showActividades();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setGrupo() {
        System.out.print("Escolha um grupo (iniciados, juniores ou séniors): ");
        grupo = in.nextLine();
    }

    private static void showActividades() throws SQLException, ClassNotFoundException{
        String[] actividadesQuery = Control.getActividadesGrupo(grupo);;
        System.out.println("Actividades\n");
        System.out.println(" | Grupo |    Designação   |  Descrição  | Hora Inicial | Hora Final ");
        int j = 0, rowSize = 5;
        for (int i = 0; i < actividadesQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + actividadesQuery[j]);
            }
            System.out.print("\n");
        }
    }


}

package Views;

import Control.Control;
import DataTransferObject.Colono;
import DataTransferObject.DTOfacade;
import DataTransferObject.Pessoa;

import java.sql.SQLException;
import java.util.Scanner;

public class AddColono {
    static Scanner in = new Scanner(System.in);
    static String nomeColono, dtnascimento, contacto, ccidadao, nomePessoa, endereco, ntelefone, email, grupo, designacao, campoFerias;
    static int monitor, escolaridade, eeducacao, equipa;
    static long cutente;


    public static boolean addColono() throws SQLException, ClassNotFoundException {
        setColonoData();
        setCampoFerias();
        setMonitor();
        insertPessoa();
        setEeducacao();
        setEquipa();
        try {
            Colono colono = DTOfacade.createColono(nomeColono, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa);
            Control.insertColono(colono);
        }catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setEeducacao() throws SQLException, ClassNotFoundException {
        eeducacao = Control.getLastPessoa(nomePessoa, endereco, ntelefone, email);
    }

    private static void setEquipa() throws SQLException, ClassNotFoundException {
        String[] equipaQuery = Control.getEquipa(monitor);
        System.out.println("Equipas disponíveis\n");
        System.out.println(" | Número | Grupo | Designacao | Monitor");
        int k = 0, rowSize = 4;
        for (int i = 0; i < equipaQuery.length / rowSize; i++) {
            for (; k < rowSize * (i+1); k++) {
                System.out.print(" | " + equipaQuery[k]);
            }
            System.out.print("\n");
        }

        System.out.println();
        System.out.print("Número da equipa pretendido: ");
        equipa = in.nextInt();
        in.nextLine();
    }

    private static void insertPessoa() throws SQLException, ClassNotFoundException {

        Pessoa pessoa = DTOfacade.createPessoa(nomePessoa, endereco,ntelefone,email);
        String[] pessoaQuery = Control.insertPessoa(pessoa);
    }

    private static void setMonitor() throws SQLException, ClassNotFoundException {
        String[] pessoaMonitorQuery = DTOfacade.getPessoaMonitorbyCampoFerias(campoFerias);
        System.out.println("Monitores disponíveis\n");
        System.out.println(" | Número | Nome ");
        int k = 0, rowSize = 2;
        for (int i = 0; i < pessoaMonitorQuery.length / rowSize; i++) {
            for (; k < rowSize * (i+1); k++) {
                System.out.print(" | " + pessoaMonitorQuery[k]);
            }
            System.out.print("\n");
        }

        System.out.println();
        System.out.print("Número do monitor pretendido: ");
        monitor = in.nextInt();
    }

    private static void setCampoFerias() throws SQLException, ClassNotFoundException {
        String [] campoferiasQuery = DTOfacade.getCampoFerias();
        System.out.println("Campos de Férias disponíveis\n");
        System.out.println(" | Código |   Nome   |   Endereco  | Localidade | Código Postal | Endereço Web | Latitude | Longitude ");
        int j = 0, rowSize = 8;
        for (int i = 0; i < campoferiasQuery.length / rowSize; i++) {
            for (; j < rowSize * (i+1); j++) {
                System.out.print(" | " + campoferiasQuery[j]);
            }
            System.out.print("\n");
        }

        System.out.print("\nCódigo do Campo de Férias pretendido: ");
        campoFerias = in.nextLine();
        System.out.println();
    }

    private static void setColonoData() {
        setNomeColono();
        setDtnascimento();
        setContacto();
        setEscolaridade();
        setCcidadao();
        setCutente();
        setNomePessoa();
        setEndereco();
        setNtelefone();
        setEmail();
    }

    private static void setEmail() {
        System.out.print("Email do encarregado de educação: ");
        email = in.nextLine();
    }

    private static void setNtelefone() {
        System.out.print("Telefone do encarregado de educação (+351xxxxxxxxx): ");
        ntelefone = in.nextLine();
    }

    private static void setEndereco() {
        System.out.print("Endereço do encarregado de educação: ");
        endereco = in.nextLine();
    }

    private static void setNomePessoa() {
        System.out.print("Nome do Encarregado de Educação: ");
        nomePessoa = in.nextLine();
    }

    private static void setCutente() {
        System.out.print("Cartão de Utente: ");
        cutente = in.nextLong();
        in.nextLine();
    }

    private static void setCcidadao() {
        System.out.print("Cartão de Cidadão: ");
        ccidadao = in.nextLine();
    }

    private static void setEscolaridade() {
        System.out.print("Escolaridade (1-12): ");
        escolaridade = in.nextInt();
        in.nextLine();

    }

    private static void setContacto() {
        System.out.print("Contacto (+351xxxxxxxxx): ");
        contacto = in.nextLine();
    }

    private static void setDtnascimento() {
        System.out.print("Data de Nascimento (aaaa-mm-dd): ");
        dtnascimento = in.nextLine();
    }

    private static void setNomeColono() {
        System.out.print("Nome do Colono: ");
        nomeColono = in.nextLine();
    }
}

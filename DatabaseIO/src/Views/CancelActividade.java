package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class CancelActividade {
    private static Scanner in = new Scanner(System.in);
    private static int referencia, equipa;

    public static boolean cancelActividade() throws SQLException, ClassNotFoundException {
        try {
            showActividadesPorEquipa();
            setReferencia();
            setEquipa();
            deleteActividade();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void showActividadesPorEquipa() throws SQLException, ClassNotFoundException {
        String[] actividadeEquipaQuery = Control.getAtividadesPorEquipas();
        System.out.println("Actividades por equipas\n");
        System.out.println(" | Referencia | Designação |  Descricao  | Hora inicial | Hora final | N. de equipa");
        int j = 0, rowSize = 6;
        for (int i = 0; i < actividadeEquipaQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + actividadeEquipaQuery[j]);
            }
            System.out.print("\n");

        }
    }

    private static void setReferencia() {
        System.out.print("Escolha a refencia da actividade a cancelar: ");
        referencia = in.nextInt();
        in.nextLine();

    }

    private static void setEquipa() {
        System.out.print("Escolha o número da equipa a cancelar: ");
        equipa = in.nextInt();
        in.nextLine();
    }

    private static void deleteActividade() throws SQLException, ClassNotFoundException {
        Control.deleteActividade(referencia, equipa);
    }
}

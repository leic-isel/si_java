package Views;

import Control.Control;

import java.sql.SQLException;

public class ChangeActividadeTimeRestriction {
    public static boolean changeActividadeTimeRestriction() throws SQLException, ClassNotFoundException {
        try {
            Control.changeActividadeTimeRestriction();
        } catch (SQLException sqlException) {
            System.out.println("Erro: " + sqlException.getMessage());
            return false;
        }
        return true;
    }
}

package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class ChangeEquipa {
    static Scanner in = new Scanner(System.in);
    static int numEquipa, numColono;

    public static boolean changeEquipa() throws SQLException, ClassNotFoundException {
        try {
            setNumColono();
            setNumEquipa();
            makeTheChange();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setNumColono() throws SQLException, ClassNotFoundException {
        String[] colonoQuery = Control.getColonos();
        System.out.println("Colonos\n");
        System.out.println(" | Número |   Nome   | Data de Nascimento ");
        int j = 0, rowSize = 3;
        for (int i = 0; i < colonoQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + colonoQuery[j]);
            }
            System.out.print("\n");
        }
        System.out.print("Escolha o número do Colono a trocar: ");
        numColono = in.nextInt();
        in.nextLine();
    }

    private static void setNumEquipa() throws SQLException, ClassNotFoundException {
        String [] equipaQuery = Control.getEquipas();
        System.out.println("Equipas\n");
        System.out.println(" | Número |   Nome   ");
        int j = 0, rowSize = 2;
        for (int i = 0; i < equipaQuery.length / rowSize; i++) {
            for (; j < rowSize * (i+1); j++) {
                System.out.print(" | " + equipaQuery[j]);
            }
            System.out.print("\n");
        }
        System.out.print("Escolha o número da equipa a colocar o colono: ");
        numEquipa = in.nextInt();
        in.nextLine();
    }

    private static void makeTheChange() throws SQLException, ClassNotFoundException {
        // TODO: garantir que não é ultrapassado o limite de participantes imposto
        Control.changeColonoFromTeam(numColono, numEquipa);
    }
}

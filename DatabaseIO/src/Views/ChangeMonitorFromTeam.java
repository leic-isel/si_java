package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class ChangeMonitorFromTeam {
    private final static Scanner in = new Scanner(System.in);
    private static int equipa, monitor, oldEquipa;

    public static boolean changeMonitor() throws SQLException, ClassNotFoundException {
        try {
            showMonitores();
            setEquipa();
            setMonitor();
            setOldEquipa();
            makeChage();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void showMonitores() throws SQLException, ClassNotFoundException {
        String[] monitoresQuery = Control.getPessoaMonitorEquipa();
        System.out.println("Monitores\n");
        System.out.println(" | Número |    Nome   |  Equipa  | Número equipa ");
        int j = 0, rowSize = 4;
        for (int i = 0; i < monitoresQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + monitoresQuery[j]);
            }
            System.out.print("\n");
        }
    }

    private static void setEquipa() {
        System.out.print("Escolha o número da equipa à qual atribuir monitor: ");
        equipa = in.nextInt();
        in.nextLine();

    }

    private static void setMonitor() {
        System.out.print("Escolha o número do monitor a atribuir à equipa: ");
        monitor = in.nextInt();
        in.nextLine();

    }

    private static void setOldEquipa() {
        System.out.print("Digite o número da equipa atual do monitor: ");
        oldEquipa = in.nextInt();
        in.nextLine();

    }

    private static void makeChage() throws SQLException, ClassNotFoundException{
        Control.changeMonitorFromTeam(oldEquipa, null);
        Control.changeMonitorFromTeam(equipa, monitor);
    }
}

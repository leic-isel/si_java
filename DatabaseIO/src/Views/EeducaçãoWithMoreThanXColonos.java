package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class EeducaçãoWithMoreThanXColonos {
    private static final Scanner in = new Scanner(System.in);
    private static int nColonos;

    public static boolean eEducacaoWithMoreThanXColonos() throws SQLException, ClassNotFoundException {
        try {
            setNcolonos();
            showEeducacao();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setNcolonos() {
        System.out.print("Número mínimo de colonos por encarregado de educação: ");
        nColonos = in.nextInt();
        in.nextLine();

    }

    private static void showEeducacao() throws SQLException, ClassNotFoundException {
        String[] actividadesQuery = Control.eEducacaoWithMoreThanXColonos(nColonos);
        System.out.println("Encarregados de educação\n");
        System.out.println(" |   Nome   | N. educandos ");
        int j = 0, rowSize = 2;
        for (int i = 0; i < actividadesQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + actividadesQuery[j]);
            }
            System.out.print("\n");
        }
    }
}

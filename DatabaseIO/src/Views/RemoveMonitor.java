package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class RemoveMonitor {
    private static final Scanner in = new Scanner(System.in);
    private static int numero;

    public static boolean removeMonitor() throws SQLException, ClassNotFoundException {
        try {
            showMonitores();
            setNumero();
            deleteMonitor();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void showMonitores() throws SQLException, ClassNotFoundException {
        String[] monitoresQuery = Control.getPessoaMonitor();
        System.out.println("Monitores\n");
        System.out.println(" | Número |   Nome  ");
        int j = 0, rowSize = 2;
        for (int i = 0; i < monitoresQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + monitoresQuery[j]);
            }
            System.out.print("\n");
        }
    }

    private static void setNumero() {
        System.out.print("Escolha o número do monitor a remover do sistema: ");
        numero = in.nextInt();
        in.nextLine();

    }

    private static void deleteMonitor() throws SQLException, ClassNotFoundException {
        Control.deleteMonitor(numero);
    }
}

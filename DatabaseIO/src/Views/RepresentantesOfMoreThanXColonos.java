package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class RepresentantesOfMoreThanXColonos {
    private static final Scanner in = new Scanner(System.in);
    private static int nColonos;

    public static boolean representantesOfMoreThanXcolonos() throws SQLException, ClassNotFoundException {
        try {
            setNcolonos();
            showRepresentantes();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setNcolonos() {
        System.out.print("Número mínimo de representantes por colonos: ");
        nColonos = in.nextInt();
        in.nextLine();

    }

    private static void showRepresentantes() throws SQLException, ClassNotFoundException {
        String[] actividadesQuery = Control.representantesOfMoreThanXColonos(nColonos);
        System.out.println("Representantes por colonos\n");
        System.out.println(" |   Representante   | N. colonos ");
        int j = 0, rowSize = 2;
        for (int i = 0; i < actividadesQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + actividadesQuery[j]);
            }
            System.out.print("\n");
        }
    }
}

package Views;

import Control.Control;

import java.sql.SQLException;
import java.util.Scanner;

public class ShowActividadeParticipantes {
    private static final Scanner in = new Scanner(System.in);
    private static int minParticipantes;

    public static boolean showParticipantes() throws SQLException, ClassNotFoundException {
        try {
            setMinParticipantes();
            show();
        } catch (SQLException qsqlex) {
            System.out.println("Erro: " + qsqlex.getMessage());
            return false;
        }
        return true;
    }

    private static void setMinParticipantes() {
        System.out.print("Escolha o número mínimo de participantes: ");
        minParticipantes = in.nextInt();
        in.nextLine();

    }

    private static void show() throws SQLException, ClassNotFoundException{
        String[] participantesQuery = Control.getMinParticipantes(minParticipantes);
        System.out.println("Actividade com um mínimo de particpantes = " + minParticipantes + "\n");
        System.out.println(" |   Actividade    |     Descrição     | Participantes");
        int j = 0, rowSize = 3;
        for (int i = 0; i < participantesQuery.length / rowSize; i++) {
            for (; j < rowSize * (i + 1); j++) {
                System.out.print(" | " + participantesQuery[j]);
            }
            System.out.print("\n");
        }
    }
}
